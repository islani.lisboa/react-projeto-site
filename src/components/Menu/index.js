import { Link } from "@reach/router";
import styled from "styled-components";

const Menu = ({ links }) => {
  return (
    <SMenu>
      <SMenuContainer>
        {links?.map((itemMenu, i) => (
          <SMenuItem key={i}>
            <SMenuLink to={itemMenu.path}>{itemMenu.title}</SMenuLink>
          </SMenuItem>
        ))}
      </SMenuContainer>
    </SMenu>
  );
};

export default Menu;

const SMenu = styled.div``;

const SMenuContainer = styled.ul`
  list-style: none;
  display: flex; ;
`;

const SMenuItem = styled.li`
  padding: 5px;
  margin: 5px 5px;
  background-color: #86c8f7;
  border-radius: 5px;
  
  &:hover {
    background-color: #34a5c9;
  }
`;

const SMenuLink = styled(Link)`
  text-decoration: none;
  color: #fff;
`;
