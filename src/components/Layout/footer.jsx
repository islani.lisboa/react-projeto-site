import styled from "styled-components";
import { Container } from "../../assets/style";

const FooterComponent = () => {
  return (
    <Footer>
      <FooterContainer>Todos os Direitos Rervados</FooterContainer>
    </Footer>
  );
};

export default FooterComponent;

const Footer = styled.div`
  background-color: #fafafa;
  border-top: 1px solid #ddd;
  padding: 5px;  
  color: #86c8f7;
  text-align: center;
`;

const FooterContainer = styled(Container)`
  justify-content: center;
`;
