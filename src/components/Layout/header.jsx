import styled from "styled-components";
import Logo from "../Logo/index";
import Menu from "../Menu/index";
import { Container } from "../../assets/style";

const MenuLinks = [
  { title: "Home", path: "/" },
  { title: "Sobre", path: "/sobre" },
];

const HeaderComponent = ({ text }) => {
  return (
    <Header>
      <Container display="row">
        <Logo></Logo>
        <Menu links={MenuLinks} />
      </Container>
    </Header>
  );
};

export default HeaderComponent;

const Header = styled.div`
  background-color: #fafafa;
  border-bottom: 1px solid #eee;
`;

