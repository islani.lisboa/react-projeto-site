import styled from "styled-components";
import { Container } from "../../assets/style";
import Footer from "./footer";
import Header from "./header";

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <Main>
        <Container>{children}</Container>
      </Main>
      <Footer />
    </>
  );
};

export default Layout;

const Main = styled.div`
  flex: 1;
`;
