import styled from "styled-components";

const Logo = ({ children }) => <SLogo dark>{children}</SLogo>;

export default Logo;

const SLogo = styled.div`
  color: #86C8F7;
  font-weight: 600;
  padding: 10px 0 ;
  flex: 1;
`;
