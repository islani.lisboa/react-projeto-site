import React from "react";
import ReactDOM from "react-dom";
import GlobalStyle from "./assets/globalStyled";
import Routers from "./router";

ReactDOM.render(
  <>
    <Routers />
    <GlobalStyle />
  </>,
  document.getElementById("root")
);
