import styled from "styled-components";

export const Container = styled.div`
  max-width: 1080px;
  display: flex;
  margin: 0 auto;
  flex-direction: ${(props) => props.display || "row"};
`;
