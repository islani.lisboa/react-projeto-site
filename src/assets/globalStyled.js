import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`

    *{
        margin:0;
        padding: 0;
        outline: 0;
    }
   #root{
    height: 100vh;
    display: flex;
    flex-direction: column;
   }
  
`;

export default GlobalStyle;
