import { Router } from "@reach/router";
import Layout from "./components/Layout";

import Home from "./views/home";
import Sobre from "./views/sobre";

const Routers = () => (
  <Layout>
    <Router>
      <Home path="/" />
      <Sobre path="/sobre" />
    </Router>
  </Layout>
);

export default Routers;
